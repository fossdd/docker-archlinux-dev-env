from archlinux/archlinux:base-devel

run pacman -Syu --noconfirm --needed git sudo

run echo "container ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

run useradd --create-home container
user container

run git clone https://aur.archlinux.org/yay /tmp/yay
workdir /tmp/yay
run makepkg -si --noconfirm
run rm -r /tmp/yay

workdir /home/container
